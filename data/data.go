package data

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
)

type Data struct {
	Feed         string
	Parsed       Parsed
	RatingsCount map[string]int
	Health       string
}

func New() *Data {
	d := new(Data)
	// Assume bad unless told otherwise.
	d.Health = "bad"
	return d
}

func (d *Data) Get() {
	resp, err := http.Get(d.Feed)
	if err != nil {
		// Unable to get feed, mark health as bad print to console.
		fmt.Printf("Unable to load json feed %s with error: %s\n", d.Feed, err.Error())
		d.Health = "bad"
		return
	}
	defer resp.Body.Close()
	parsed_copy := d.Parsed
	err = json.NewDecoder(resp.Body).Decode(&d.Parsed)
	if err != nil {
		// Unable to decode mark health as bad and print to console.
		fmt.Printf("Unable to marshal json feed %s with error: %s\n", d.Feed, err.Error())
		d.Health = "bad"
		// Continue to serve last working copy.
		d.Parsed = parsed_copy
		return
	}
	d.Health = "good"
	d.SetRatings()
}

func (d *Data) SetRatings() {
	d.RatingsCount = map[string]int{
		"1": 0,
		"2": 0,
		"3": 0,
		"4": 0,
		"5": 0,
	}
	for _, item := range d.Parsed.Items {
		rating := strconv.Itoa(item.Rating)
		d.RatingsCount[rating]++
	}
}

type Parsed struct {
	Items []struct {
		ComputedBrowser struct {
			Browser     string `json:"Browser"`
			Version     string `json:"Version"`
			Platform    string `json:"Platform"`
			Fullbrowser string `json:"FullBrowser"`
		} `json:"computed_browser"`
		Geo struct {
			Country string  `json:"country"`
			Region  string  `json:"region"`
			City    string  `json:"city"`
			Lat     float64 `json:"lat"`
			Lon     float64 `json:"lon"`
		} `json:"geo"`
		Labels []string `json:"labels"`
		Rating int      `json:"rating"`
	} `json:"items"`
}
