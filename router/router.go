package router

import (
	"bitbucket.org/jasonrsmith/usabilla-api/data"
	"encoding/json"
	"net/http"
	"os"
	"strings"
)

type Router struct {
	PublicDirectory string
	Data            *data.Data
}

func New() *Router {
	return new(Router)
}

func (router *Router) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	pathvalues := strings.Split(r.URL.Path, "/")
	pathvalues = pathvalues[1:]
	switch pathvalues[0] {
	case "":
		d := router.PublicDirectory
		http.ServeFile(w, r, d)

	case "local_components":
		fallthrough
	case "bower_components":
		d := router.PublicDirectory
		f := d + r.URL.Path
		info, err := os.Stat(f)
		if err != nil {
			if os.IsNotExist(err) {
				http.NotFound(w, r)
				return
			}
		}
		if info.IsDir() {
			http.NotFound(w, r)
			return
		}
		http.ServeFile(w, r, f)

	case "api_endpoint":
		if len(pathvalues) == 1 {
			http.NotFound(w, r)
			return
		}
		router.APIRouter(w, r, pathvalues)
	default:
		http.NotFound(w, r)
	}
	return
}

func (router *Router) APIRouter(w http.ResponseWriter, r *http.Request, pathvalues []string) {
	// Default will be Json
	w.Header().Set("Content-Type", "application/json")

	switch pathvalues[1] {
	case "download.json":
		w.Header().Set("Content-Type", "application/octet-stream")
		APIServe(w, router.Data.Parsed)
	case "reduced_json":
		APIServe(w, router.Data.Parsed)
	case "ratings_count":
		APIServeNamed(w, router.Data.RatingsCount, "ratings_count")
	case "both":
		both := map[string]interface{}{"items": router.Data.Parsed.Items, "ratings_count": router.Data.RatingsCount}
		APIServe(w, both)
	case "health_check":
		APIServeNamed(w, router.Data.Health, "health")
	case "refresh":
		router.Data.Get()
		APIServe(w, nil)
	default:
		http.NotFound(w, r)
	}
	return
}

func APIServe(w http.ResponseWriter, values interface{}) {
	js, err := json.Marshal(values)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Write(js)
}

func APIServeNamed(w http.ResponseWriter, values interface{}, name string) {
	named := map[string]interface{}{name: values}
	APIServe(w, named)
}
