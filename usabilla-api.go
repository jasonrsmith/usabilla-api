package main

import (
	"bitbucket.org/jasonrsmith/usabilla-api/data"
	"bitbucket.org/jasonrsmith/usabilla-api/router"
	"errors"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
)

func main() {
	router := router.New()
	router.Data = data.New()
	var port string
	flag.StringVar(
		&router.PublicDirectory,
		"directory",
		"",
		"Public Directory for serving static files. *Required")
	flag.StringVar(
		&port,
		"port",
		"8080",
		"The port to serve the application.")
	flag.StringVar(
		&router.Data.Feed,
		"feed",
		"http://cache.usabilla.com/example/apidemo.json",
		"URL for json feed.")
	flag.Parse()
	if len(router.PublicDirectory) == 0 {
		log.Fatal(errors.New(`Directory for static files required.
		Please use directory flag (--directory) to specify location of static files.`))
	}

	info, err := os.Stat(router.PublicDirectory)
	if err != nil {
		log.Fatal(fmt.Errorf("Error loading directory: %s", err.Error()))
		return
	}
	if !info.IsDir() {
		log.Fatal(fmt.Errorf("This path is not a diretcory"))
		return
	}
	router.Data.Get()
	fmt.Println("Running server")
	fmt.Printf("Please visit http://localhost:%s\n", port)
	http.ListenAndServe(":"+port, router)
}
